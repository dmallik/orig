package main

import (
	"fmt"
	"log"
	"os"

	"github.com/olivere/elastic"

	"./accessors"
	"./clients"
	"./handler"
	"./router"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	STATIC_DIR = "/static/"
)

func ddbStreamHandler() {
	svc, err := elastic.NewClient(
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
		elastic.SetURL(fmt.Sprintf("https://%s", os.Getenv("ELASTICSEARCH_URL"))),
	)
	if err != nil {
		log.Printf("Error while creating ES client: %v", err)
		return
	}
	esSync := handler.EsSyncLambda{svc}
	lambda.Start(esSync.Handler)
}

func createEsClient() *accessors.Elasticsearch {
	elasticClient, _ := elastic.NewClient(
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
		elastic.SetURL(fmt.Sprintf("https://%s", os.Getenv("ELASTICSEARCH_URL"))),
	)
	var esclient = new(accessors.Elasticsearch)
	esclient.Client = elasticClient
	return esclient
}
func apiGatewayHandler() {
	// Initialize a session that the SDK will use to load
	// credentials from the shared credentials file ~/.aws/credentials
	// and region from the shared configuration file ~/.aws/config.
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Initialze AWS resources
	svc := dynamodb.New(sess)
	s3 := s3.New(sess)
	esclient := createEsClient()

	//Initialize accessors
	dynamoClient := clients.DynamoClient{svc}
	s3Accessor := accessors.S3Accessor{s3}

	// Initialize the handlers
	feedHandler := handler.FeedHandler{&dynamoClient, esclient}
	picHandler := handler.PicHandler{&s3Accessor}
	userHandler := handler.UserHandler{&s3Accessor, &dynamoClient}
	commentHandler := handler.CommentHandler{&dynamoClient}
	feedStatHandler := handler.FeedStatHandler{&dynamoClient}
	feedMediaHandler := handler.FeedMediaHandler{&s3Accessor}

	globalRouter := router.GlobalRouter{&feedHandler, &picHandler,
		&userHandler, &commentHandler, &feedStatHandler, &feedMediaHandler}

	lambda.Start(globalRouter.HandleRequest)
}

func main() {
	apiGatewayHandler()
}
